## Example - TFJob from a Pipeline

### What is it about?
Run a distributed training job using TFJob, from a pipeline.

### How to run?
- Run notebook cells
- Download created yaml file
- Upload pipeline to https://ml.cern.ch/_/pipeline/#/pipelines
